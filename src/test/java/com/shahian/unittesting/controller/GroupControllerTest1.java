package com.shahian.unittesting.controller;

import com.shahian.unittesting.model.controller.GroupController;
import com.shahian.unittesting.model.entity.Group;
import com.shahian.unittesting.model.service.intrface.GroupService;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(GroupController.class)
public class GroupControllerTest1 {
//    @Autowired
//    private MockMvc mvc;
//
//    @Test
//    public void whenGetGroups_shouldReturnMockedObject() throws Exception {
//        List<Group> groups = new ArrayList<>();
//        groups.add(new Group("Add new Group1", "desc Group1"));
//        groups.add(new Group("Add new Group2", "desc Group2"));
//        groups.add(new Group("Add new Group3", "desc Group3"));
//        groups.add(new Group("Add new Group4", "desc Group4"));
//        RequestBuilder request= MockMvcRequestBuilders.get("/api/v1/otherGroups");
//        MvcResult result=mvc.perform(request).andReturn();
//        Assertions.assertEquals(groups,result.getResponse().getOutputStream());
//    }
//    @Test
//    public void whenGetString_shouldReturnString() throws Exception {
//
//        RequestBuilder request= MockMvcRequestBuilders.get("/api/v1/returnString");
//        MvcResult result=mvc.perform(request).andReturn();
//        Assertions.assertEquals("hello",result.getResponse().getContentAsString());
//    }

}
