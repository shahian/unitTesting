package com.shahian.unittesting.controller;

import com.shahian.unittesting.model.controller.GroupController;
import com.shahian.unittesting.model.entity.Group;
import com.shahian.unittesting.model.service.intrface.GroupService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(GroupController.class)
public class GroupControllerTest {
    @Autowired
    private MockMvc mvc;
    @MockBean
    private GroupService groupService;

    @Test
    public void whenGetGroupByTitle_shouldReturnMockedObject() throws Exception {
        String title = "Group1";
        Group group = new Group();
        group.setTitle("Group1");
        group.setDescription("desc Group1");
        given(groupService.getGroupByTitle(title)).willReturn(group);
        mvc.perform(get("/api/v1/groupTitle").param("title", title)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        //.andExpect(jsonPath("$.title").value(group.getTitle()));
    }

    @Test
    public void whenGetGroups_shouldReturnMockedObjects() throws Exception {
        List<Group> groups = new ArrayList<>();
        groups.add(new Group("Add new Group1", "desc Group1"));
        groups.add(new Group("Add new Group2", "desc Group2"));
        groups.add(new Group("Add new Group3", "desc Group3"));
        groups.add(new Group("Add new Group4", "desc Group4"));
        mvc.perform(get("/api/v1/otherGroups")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
                //.andExpect(result -> when(groups).thenReturn(groups));

    }
}
